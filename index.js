/* jshint node:true */

'use strict';

var assert = require('assert');

var types = require(__dirname + '/types');
var attributes = require(__dirname + '/attributes');

function extend(a, b) {
	Object.keys(b).forEach(function(prop) {
		a[prop] = b[prop];
	});
	return a;
}

module.exports = function(validator) {
	assert(validator && validator.attributes && validator.types, 'Missing / invalid validator');

	validator.types = Object.create(validator.types);
	extend(validator.types, types);

	validator.attributes = Object.create(validator.attributes);
	extend(validator.attributes, attributes);

	return validator;
};