/* jshint node:true */

'use strict';

exports.validate = function customValidate(instance, schema, opt, ctx) {
	if (instance || schema.required) {
		var fn = schema.validate;

		if (Array.isArray(fn)) {
			for (var i = 0, len = fn.length; i < len; i += 1) {
				var res = fn[i](instance, schema, opt, ctx);
				if (res) { return res; }
			}
		} else {
			return fn(instance, schema, opt, ctx);
		}
	}
};