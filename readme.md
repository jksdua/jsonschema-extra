# jsonschema-extra

[![NPM version][npm-image]][npm-url]
[![David deps][david-image]][david-url]
[![node version][node-image]][node-url]
[![npm download][download-image]][download-url]

[npm-image]: https://img.shields.io/npm/v/jsonschema-extra.svg?style=flat-square
[npm-url]: https://npmjs.org/package/jsonschema-extra
[travis-image]: https://img.shields.io/travis/jksdua/jsonschema-extra.svg?style=flat-square
[travis-url]: https://travis-ci.org/jksdua/jsonschema-extra
[coveralls-image]: https://img.shields.io/coveralls/jksdua/jsonschema-extra.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/jksdua/jsonschema-extra?branch=master
[david-image]: https://img.shields.io/david/jksdua/jsonschema-extra.svg?style=flat-square
[david-url]: https://david-dm.org/jksdua/jsonschema-extra
[node-image]: https://img.shields.io/badge/node.js-%3E=_0.10-green.svg?style=flat-square
[node-url]: http://nodejs.org/download/
[download-image]: https://img.shields.io/npm/dm/jsonschema-extra.svg?style=flat-square
[download-url]: https://npmjs.org/package/jsonschema-extra

**Extends jsonschema validator with some common custom types and attributes**


## Usage

### Installation

```bash
npm install jsonschema-extra --save
```

### Example

```js
var jsonschema = require('jsonschema');
var extra = require('jsonschema-extra');

var validator = new (jsonschema.Validator)();
extra(validator);

// regexp
validator.validate(/abc/, { type: 'regexp' });

// error
validator.validate(new Error(), { type: 'error' });

// mongodb objectid
validator.validate('123456789012345678901234', { type: 'objectId' });
```


## Attributes

See `jsonschema` documentation for more detailed documentation on custom attributes.

### validate

Custom validator when `jsonschema` just isn't what you are after. This one is my absolute favourite! It comes in handy whenever you are trying to do something complex.

> One of the features of jsonschema is that you can share it across client and server which is exactly what I do! However, if your schema contains a validate attribute it will no longer be valid json. This is not a big issue if you are ok for client side code to not contain all your validation logic if schema is sent from the server. 

**Single validator**

```js
var schema = {
	validate: function(instance, schema, options) {
		// do your crazy validation here
			// return an error string if not valid
		return 'is not a valid instance';
	}
};
```

**Multiple validators**

```js
var schema = {
	validate: [
		function(instance, schema, options) {
			// do your crazy validation here
				// return an error string if not valid
			return 'is not a valid instance';
		},
		function(instance, schema, options) {
			// do your crazy validation here
				// return an error string if not valid
			return 'is not a valid instance';
		}
	]
};
```


## Types

Supported types:

- error (instanceof Error)
- regexp (instanceof RegExp)
- function (Function - also works for Generator Function)
- generatorFunction (ES6 Generator Function)
- objectId (MongoDb objectId)
- plainObject (calls _.isPlainObject)

```js
validator.validate(new Error(), { type: 'error' });

validator.validate(/regexp/, { type: 'regexp' });

validator.validate(function() {}, { type: 'function' });

validator.validate(function *() {}, { type: 'generatorFunction' });

validator.validate('123456789012345678901234', { type: 'objectId' });
validator.validate(new ObjectID(), { type: 'objectId' });

validator.validate({ a: 'a', b: 'b' }, { type: 'plainObject' });
```

### js-quantities types

Quantity types supported by [js-quantities](https://www.npmjs.com/package/js-quantities)

- qty.acceleration
- qty.activity
- qty.angle
- qty.angular_velocity
- qty.area
- qty.capacitance
- qty.charge
- qty.conductance
- qty.currency
- qty.current
- qty.energy
- qty.force
- qty.frequency
- qty.illuminance
- qty.inductance
- qty.length
- qty.luminous_power
- qty.magnetism
- qty.mass
- qty.mass_concentration
- qty.memory
- qty.molar_concentration
- qty.potential
- qty.power
- qty.pressure
- qty.radiation
- qty.resistance
- qty.speed
- qty.substance
- qty.temperature
- qty.time
- qty.unitless
- qty.viscosity
- qty.volume

```js
validator.validate('1 meter', { type: 'qty.length' });

validator.validate('45.3 seconds', { type: 'qty.time' });


// can also validate Qty instances
var Qty = require('js-quantities');
var qty = Qty.parse('2 meters');

validator.validate(qty, { type: 'qty.length' });
```


## Testing

Install `mocha` globally

```bash
$ npm install mocha -g
```

Run tests

```bash
$ npm test
```


## Changelog

### v1.2.0 (22 Dec 2014)
- Added quantity types backed by [js-quantities](https://www.npmjs.com/package/js-quantities)

### v1.1.1 (19 Dec 2014)
- Minor doc updates

### v1.1.0 (18 Sep 2014)
- Allow multiple validators in the `validate` property

### v1.0.2 (30 Sep 2014)
- Fixed so it works on non-Windows systems
- Removed mongodb dev dependency

### v1.0.1 (27 Sep 2014)
- Updated docs
- Updated package.json

### v1.0.0 (27 Sep 2014)
- Changed public API - jsonschema is no longer a dependency. This lets jsonschema to be updated in userland without this module requiring an update.
- Name of module changed to jsonschema-extra
- Removed conditionalEnum custom attribute since this has been fixed in the latest versions of jsonschema
- Removed `speed` type
