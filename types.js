/* jshint node:true */

'use strict';

var Qty = require('js-quantities');
var isPlainObject = require('lodash.isplainobject');

exports.error = function testError(er) {
	return (er instanceof Error);
};

exports.regexp = function testRegexp(re) {
	return (re instanceof RegExp);
};

exports.function = function testFunction(fn) {
	return ('function' === typeof fn);
};

exports.generatorFunction = function testGeneratorFunction(fn) {
	return ('function' === typeof fn && 'GeneratorFunction' === fn.constructor.name);
};

exports.objectId = function testObjectId(id) {
	// coerce to string so the function can be generically used to test both strings and native objectIds created by the driver
	id = id + '';
	var len = id.length, valid = false;
	if (len == 12 || len == 24) {
		valid = /^[0-9a-fA-F]+$/.test(id);
	}
	return valid;
};

exports.plainObject = function testPlainObject(ob) {
	return isPlainObject(ob);
};


// quantities
var QTY_PREFIX = 'qty.';

function isKind(kind) {
	return function(qty) {
		qty = (qty instanceof Qty ? qty : Qty.parse('' + qty));

		return (qty && kind === qty.kind());
	};
}

Qty.getKinds().forEach(function(kind) {
	exports[QTY_PREFIX + kind] = isKind(kind);
});