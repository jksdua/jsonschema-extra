/* jshint node:true */
/* globals describe, it */

'use strict';

describe('#jsonschema-extra', function() {
	var assert = require('assert');

	var validator = new (require('jsonschema').Validator)();
	require(__dirname)(validator);

	var extras = {
		types: require(__dirname + '/types'),
		attributes: require(__dirname + '/attributes')
	};

	function startsWith(string, searchString, position) {
		position = position || 0;
		return string.lastIndexOf(searchString, position) === position;
	}

	var assertions = {
		types: {
			error: [
				{
					instance: 123,
					pass: false
				},
				{
					instance: new Error(),
					pass: true
				}
			],
			regexp: [
				{
					instance: 'abc',
					pass: false
				},
				{
					instance: /a/,
					pass: true
				},
				{
					instance: new RegExp('a', 'i'),
					pass: true
				}
			],
			function: [
				{
					instance: new Function(), // jshint ignore:line
					pass: true
				},
				{
					instance: eval('function a() {}'), // jshint ignore:line
					pass: true
				},
				{
					instance: function() {},
					pass: true
				},
				{
					instance: function *() {}, // jshint ignore:line
					pass: true
				}
			],
			generatorFunction: [
				{
					instance: function() {},
					pass: false
				},
				{
					instance: function *() {}, // jshint ignore:line
					pass: true
				}
			],
			objectId: [
				{
					instance: 123,
					pass: false
				},
				{
					instance: '123456789012345678901234',
					pass: true
				}
			],
			plainObject: [
				{
					instance: 123,
					pass: false
				},
				{
					instance: new String('abc'), // jshint ignore:line
					pass: false
				},
				{
					instance: { a: 1 },
					pass: true
				},
				{
					instance: { a: { a: 1 } },
					pass: true
				}
			],
			'qty.speed': [
				{
					instance: 123,
					pass: false
				},
				{
					instance: '123 kph',
					pass: true
				}
			]
		},
		attributes: {
			validate: [
				{
					instance: 1,
					schema: {
						validate: function() {
							return 'is not valid';
						}
					},
					pass: false
				},
				{
					instance: 2,
					schema: {
						validate: function() {
							return;
						}
					},
					pass: true
				},
				{
					instance: 3,
					schema: {
						validate: [
							function() {
								return 'is not valid';
							},
							function() {
								return;
							}
						]
					},
					pass: false
				},
				{
					instance: 4,
					schema: {
						validate: [
							function() {
								return;
							},
							function() {
								return 'is not valid';
							}
						]
					},
					pass: false
				},
				{
					instance: 5,
					schema: {
						validate: [
							function() {
								return;
							},
							function() {
								return;
							}
						]
					},
					pass: true
				}
			]
		}
	};

	it('should test everything', function() {
		Object.keys(extras).forEach(function(category) {
			Object.keys(extras[category]).forEach(function(name) {
				if (!startsWith(name, 'qty.')) {
					assert(
						name in assertions[category],
						'Missing assertions for ' + category + '.' + name
					);
				}
			});
		});
	});

	describe('#types', function() {
		Object.keys(assertions.types).forEach(function(type) {
			var category = assertions.types[type];
			var schema = { type: type };

			describe('#' + type, function() {
				category.forEach(function(assertion) {
					it('should ' + (assertion.pass ? '' : 'not ') + 'work for ' + assertion.instance, function() {
						var res = validator.validate(assertion.instance, schema);
						assert.equal(res.valid, assertion.pass, res.errors);
					});
				});
			});

		});
	});

	describe('#attributes', function() {
		Object.keys(assertions.attributes).forEach(function(attribute) {
			var category = assertions.attributes[attribute];

			describe('#' + attribute, function() {
				category.forEach(function(assertion) {
					it('should ' + (assertion.pass ? '' : 'not ') + 'work for ' + assertion.instance, function() {
						var res = validator.validate(assertion.instance, assertion.schema);
						assert.equal(res.valid, assertion.pass, res.errors);
					});
				});
			});

		});
	});
});